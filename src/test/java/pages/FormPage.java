package pages;

import org.openqa.selenium.By;

import metodos.MetodosDeTestes;

public class FormPage {
	
	MetodosDeTestes metodo = new MetodosDeTestes();
	By name = By.id("name");
	By lastName= By.id("lastName");
	By dateNasc = By.id("dateNasc");
	By whatsapp = By.id("phoneNumber");
	By btnEnviar = By.xpath("//button[text()='Enviar']");
	
	public void cadastroForm(String name, String lastName, String dateNasc, String whatsapp, String textoAlert, String nameEvidencia) {
		metodo.escrever(this.name, name);
		metodo.escrever(this.lastName, lastName);
		metodo.escrever(this.dateNasc, dateNasc);
		metodo.escrever(this.whatsapp, whatsapp);
		metodo.clicar(btnEnviar);
		metodo.validarAlert(textoAlert);
		metodo.capturarScreenShot("Cadastro", nameEvidencia);
		
	}

}
