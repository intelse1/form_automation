package steps;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import pages.FormPage;
import runner.Executa;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestForm {
	
	FormPage form = new FormPage();
	
	
	@Before
	public void iniciandoTeste() {
    	Executa.openBrowser();		
	}
    
    @After
    public void testeFinalizado() {
    	Executa.exitBrowser();
    }
    
    @Test
    public void cadastroForm() {
    	String testName = new Object() {}.getClass().getEnclosingMethod().getName();
    	String msgAlert = "Cadastro realizado com sucesso!\n" + "\n" + "Nome: Joao\n" + "Sobrenome: Gabriel\n" + "Data de Nascimento: 1989-02-04\n" + "Whatsapp: (18)999787708" ;
    	form.cadastroForm("Joao", "Gabriel", "1989-02-04", "(18)999787708", msgAlert, testName);
    }
   
    @Test
    public void cadastroForm1() {
    	String testName = new Object() {}.getClass().getEnclosingMethod().getName();
    	String msgAlert = "Cadastro realizado com sucesso!\n" + "\n" + "Nome: Dior\n" + "Sobrenome: Ribeiro\n" + "Data de Nascimento: 1999-03-20\n" + "Whatsapp: (18)999787708" ;
    	form.cadastroForm("Dior", "Ribeiro", "1999-03-20", "(18)999787708", msgAlert, testName);
    }
    
    @Test
    public void cadastroForm2() {
    	String testName = new Object() {}.getClass().getEnclosingMethod().getName();
    	String msgAlert = "Cadastro realizado com sucesso!\n" + "\n" + "Nome: Gabriel\n" + "Sobrenome: Joao\n" + "Data de Nascimento: 1990-04-02\n" + "Whatsapp: (18)999787708" ;
    	form.cadastroForm("Gabriel", "Joao", "1990-04-02", "(18)999787708", msgAlert, testName);
    }

}
