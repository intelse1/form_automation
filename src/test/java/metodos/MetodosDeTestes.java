package metodos;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import drivers.DriverFactory;

public class MetodosDeTestes extends DriverFactory{
	
	public void escrever(By elemento, String texto) {
		driver.findElement(elemento).sendKeys(texto);
	}

	public void clicar(By elemento) {
		driver.findElement(elemento).click();
	}
	
	public void validarAlert(String msgAlert) {
		String alert = driver.switchTo().alert().getText();
		System.out.println(alert);
		assertTrue(alert.contains(msgAlert));
		driver.switchTo().alert().accept();
	}
	
	public void capturarScreenShot(String story, String evidencia) {
		File screen= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screen, new File("./evidencia/" + story + "/" + evidencia +  ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
